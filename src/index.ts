import { Config, createConfig } from "./Config"
import { Cell, CellUtils } from "./Cell"
import { Point, PointUtils } from "./Point"
import { GameMap, createMap } from "./GameMap"
import { createPainter } from "./Painter";
import { createGroupSolver } from "./GroupSolver";

const config = createConfig(700, 700, 50);
const painter = createPainter(config);
const map = createMap(config);
const groupSolver = createGroupSolver(map, config);

painter.drawMap(map);
painter.onClick(handleClick);

function handleClick(e: MouseEvent) {
  const { clientX, clientY } = e;
  const { size, width } = config;
  const x = Math.floor(clientX / size);
  const y = Math.floor(clientY / size);

  const group = groupSolver.groupAtPoint({ x, y });
  painter.drawGroup(group);
}