interface Config {
  width: number;
  height: number;
  size: number;
  rows: number;
  cols: number;
}
const createConfig = (width: number, height: number, size: number): Config => ({
  width,
  height,
  size,
  rows: height / size,
  cols: height / size
})

export { Config, createConfig };