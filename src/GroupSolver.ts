import { Point, PointUtils } from "./Point";
import { GameMap } from "./GameMap";
import { Config } from "./Config";
import { Cell } from "./Cell";

const createGroupSolver = (map: GameMap, config: Config) => {
  const { pointToIndex, pointOutOfBounds } = PointUtils(config);
  const cellAtPoint = (p: Point): Cell => map[pointToIndex(p)];

  const groupAtPoint = (p: Point): Set<number> => {
    const group = new Set<number>();
    group.add(pointToIndex(p));

    const groupType = cellAtPoint(p)
    const neighbours = [[1, 0], [-1, 0], [0, 1], [0, -1]];

    const checkNeighbours = ({ x, y }: Point) => {
      neighbours.forEach(relativePos => {
        const neighbourPoint = { x: x + relativePos[0], y: y + relativePos[1] }
        if (pointOutOfBounds(neighbourPoint)) {
          return;
        }
        const neighbourIndex = pointToIndex(neighbourPoint);
        const type = cellAtPoint(neighbourPoint);
        if (type === groupType && !group.has(neighbourIndex)) {
          group.add(neighbourIndex);
          checkNeighbours(neighbourPoint);
        }
      })
    }
    checkNeighbours(p);
    return group;
  }

  return { groupAtPoint };
}

export { createGroupSolver }