import { Cell } from "./Cell"
import { Config } from "./Config"

type GameMap = Cell[];
const createMap = (config: Config): GameMap => {
  const { width, height, size, rows, cols } = config;
  const types: Cell[] = [0, 1, 2, 3];
  const map: Cell[] = [];
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      const cell = Math.round(Math.random() * (types.length - 1));
      map.push(cell);
    }
  }
  return map;
}

export { GameMap, createMap }