import { Config } from "./Config";

type Point = { x: number; y: number }
const PointUtils = (config: Config) => ({
  pointToIndex: ({ x, y }: Point) => x + y * (config.width / config.size),
  indexToPoint: (i: number) => {
    const { width, height, size, rows, cols } = config;
    const x = i % rows;
    const y = Math.floor(i / cols);
    return { x, y };
  },
  pointOutOfBounds: ({ x, y }: Point) => {
    const { width, height, size, rows, cols } = config;
    return x >= cols || y >= rows || x < 0 || y < 0;
  }
})

export { Point, PointUtils }