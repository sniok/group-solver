import { GameMap } from "./GameMap"
import { Config } from "./Config";
import { CellUtils } from "./Cell";
import { PointUtils } from "./Point";

interface Painter {
  drawMap(map: GameMap): void;
  drawGroup(group: Set<number>, color?: string): void,
  onClick(callback: (e: MouseEvent) => void): void,
}
const createPainter = (config: Config): Painter => {
  const canvas = document.querySelector('canvas');
  if (canvas === null) {
    throw new Error("Canvas not found");
  }
  const ctx = canvas.getContext('2d');
  if (ctx === null) {
    throw new Error("Something went horribly wrong");
  }

  const drawMap = (map: GameMap) => {
    const { width, height, size } = config;
    const rows = height / size;
    const cols = width / size;
    ctx.clearRect(0, 0, width, height);
    map.forEach((cellType, i) => {
      const x = i % rows;
      const y = Math.floor(i / cols);
      ctx.fillStyle = CellUtils.color(cellType);
      ctx.fillRect(x * size, y * size, size, size)
    })
  }

  const onClick = (callback: (e: MouseEvent) => void) => {
    canvas.addEventListener('click', callback);
  }

  const drawGroup = (group: Set<number>, color: string = 'white') => {
    const { indexToPoint } = PointUtils(config);
    for (let cellIndex of group.values()) {
      const { x, y } = indexToPoint(cellIndex);
      const { size } = config;

      ctx.fillStyle = color;
      ctx.fillRect(x * size, y * size, size, size);
    }
  }

  return {
    drawMap,
    onClick,
    drawGroup
  }
}

export { Painter, createPainter }